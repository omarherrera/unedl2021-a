import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExpresionesRegulares {
    public static void main(String[] args) {
        valorCURP();
    }

    public static void valorCURP(){
        Pattern pat2 = Pattern.compile("[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}(1[0-2]|0[1-9])(3[01]|[12][0-9]|0[1-9])[HM]{1}[A-Z]{2}[^AEIOU]{3}[0-9]{2}");
        List<String> curps = new ArrayList();
        curps.add("ROVI490617HSPDSS05");
        curps.add("PERC561125MSPRMT03");
        curps.add("TOHA530902HSPRRN07");
        curps.add("HEOM561232HJCRRG03");
        curps.add("VERM580121MJCRNR07");
        curps.add("BFNR991230");

        for(String date2 : curps)
        {
            Matcher matcher = pat2.matcher(date2);
            System.out.println(date2 +" : "+ matcher.matches());
        }
    }

    public static void valorIdentificador(){
        Pattern pat3 = Pattern.compile("([a-zA-Z]{1})([a-zA-Z0-9_\\.]{1,254})?");
        List<String> identificadores = new ArrayList();
        identificadores.add("x");
        identificadores.add("numero1");
        identificadores.add("numero_1");
        identificadores.add("numero.1");
        identificadores.add("Numero1");
        identificadores.add("este_es_un_modificaDOR_Mas_largo");
        identificadores.add("1sSte_es_un_modificaDOR_Mas_largo");

        for(String iden : identificadores)
        {
            Matcher matcher = pat3.matcher(iden);
            System.out.println(iden +" : "+ matcher.matches());
        }
    }

    public static void valorFecha(){//yyyy-mm-dd
        Pattern pat3 = Pattern.compile("((19[0-9]{2})|(200|201)[0-9]{1}|(202[01]{1}))-(1[0-2]|0[1-9])-(3[01]|[12][0-9]|0[1-9])");
        List<String> fechas = new ArrayList();
        fechas.add("2011-07-13");
        fechas.add("2011-13-07");
        fechas.add("2021-07-13");
        //Missing leading zeros
        fechas.add("11-1-1");
        fechas.add("2011-1-01");
        fechas.add("2011-12-31");

        for(String fec : fechas)
        {
            Matcher matcher = pat3.matcher(fec);
            System.out.println(fec +" : "+ matcher.matches());
        }
    }

}

| Comando     | Descripción | 
| :---        |    :----   |
| use      | Crear o usar una base de datos       |
| db       | Saber que base de datos esta en uso        |
| insert   | Insertar datos en una tabla       |
| remove({condicion})   | Elimina el dato filtrado        |
| remove() | Elimina todos los datos de la coleccióm        |
| drop()   | Elimina una coleccion       |
| createCollection()    | Crear una colección dentro de la base de datos       |
| show dbs | Mostrar bases de datos actuales       |
| show collection       | Mostrar colecciones dentro de una BD        |
| find()   | Mostrar los datos filtrados  |
| find().pretty()  |  Mostrar los datos filtrados con una estructura mas legible  |
| help()   | Obtener comandos de ayuda en mongo       |
| db.help()             | Obtener comandos de ayuda acerca de una base de datos en mongo    |
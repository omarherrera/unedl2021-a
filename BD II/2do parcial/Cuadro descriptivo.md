﻿## Cuadro descriptivo 

|Operaciones|Descripcion                                                                          |
|-----------|-------------------------------------------------------------------------------------|
|$project   |Modifica el tipo de dato entrante (Añade, elimina, recalcula).                       |
|$match     |Filtra la entrada, sólo con campos que cumplan la condición.                         |
|$limit     |Reduce el número de resultados mostrados.				                              |
|$skip		|Ignora un número dado de registros para no mostrarlos.                               |
|$unwind    |Es utilizado en arreglos. Desagrupa los datos.                                       |
|$group     |Agrupa documentos según una determinada condición.                                   |
|$sort		|Ordena los documentos por un campo dado, ascendente o descendente.                   |
|$geoNear   |Es utilizado en datos geo-espaciales. Devuelve un dato según el punto de ubicación.  |
|$redact    |Permite recorrer a profundidad ciertos campos de un documento y realiza una acción.  |
|$out       |Almacena los resultados de un proceso en una colección indicada.                     |
